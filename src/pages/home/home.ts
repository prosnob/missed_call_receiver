import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { CallLog,CallLogObject } from '@ionic-native/call-log';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  call: string = "in";
  filters: CallLogObject[];
  recordsFound: any;
  constructor(public navCtrl: NavController, private callLog: CallLog, public http: HttpClient, private platform: Platform) {
    //check and ask permission to get call log
    this.platform.ready().then(() => {
      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
            this.getContacts("type","1","==");
          })
          .catch(e => alert(" requestReadPermission " + JSON.stringify(e)));
        } 
        else {
          this.getContacts("type", "1", "==");
        }
      })
      .catch(e => alert(" hasReadPermission " + JSON.stringify(e)));
    });
    this.registerMissedCall();
  }
  getContacts(name, value, operator) {
    //Getting time before now 1 hour
    var today = new Date();
    today.setHours(today.getHours()-1);
    var fromTime = today.getTime();

    this.filters = [
      {
        name: name,
        value: value,
        operator: operator,
      }, 
      {
        name: "date",
        value: fromTime.toString(),
        operator: ">",
      }
    ];
    this.callLog.getCallLog(this.filters)
    .then(results => {
      this.recordsFound = results;
    })
    .catch(e => alert(" LOG " + JSON.stringify(e)));
  }

  registerMissedCall(){
    let newMissedCalls = [];
    let currentMissedCalls = [];
    let oldMissedCalls = [];
    setInterval(
      ()=>{
        try {
          //Getting time before now 1 minute
          let today = new Date();
          today.setMinutes(today.getMinutes()-1);
          let  fromTime = today.getTime();
          //filters condition get missed call
          this.filters = [
            {
            name: 'type',
            value: '3',
            operator: '==',
            },{
            name: "date",
            value: fromTime.toString(),
            operator: ">",
            }
          ];
          this.callLog.getCallLog(this.filters)
          .then(results => {
            //filters to find new missed call
            if(currentMissedCalls.length === 0 && oldMissedCalls.length === 0){
              oldMissedCalls = results;
              newMissedCalls = results;
            }else{
              currentMissedCalls = results;
              newMissedCalls = currentMissedCalls.filter(({ date: currentTime  }) => !oldMissedCalls.some(({ date: oldTime }) => currentTime === oldTime));
              oldMissedCalls = currentMissedCalls;
            }
            for (let missedCall of newMissedCalls){
              let tel = {
                phone: missedCall.number,
                missed_timestamp:missedCall.date
              };
              this.http.post('http://192.168.88.177:3000/missed-call/create',tel).subscribe(data => {
                console.log(data);
              }, 
              err => {
                console.log(err);
              });
            };
          })
          .catch(e => alert(" LOG " + JSON.stringify(e)));
        } catch (error) {};
      },500
    );
  }

  formateDate(timestamp:number){
    let date = new Date(timestamp);
    return date.toLocaleString();
  }
}
